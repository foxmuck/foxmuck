mod net;

use std::error;
use tokio::prelude::*;

type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

fn get_command_string(buf: String) -> Option<String> {
    let command = match buf.split_whitespace().next() {
        Some(command) => Some(command.to_string()),
        None => None,
    };

    return command;
}

#[tokio::main]
async fn main() -> Result<()> {
    let mut ln = match net::socket::create_listener(8888, false).await {
        Ok(ln) => ln,
        Err(e) => return Err(e.into()),
    };

    println!("Socket listening on {}", ln.local_addr().unwrap());

    loop {
        let (mut socket, _) = ln.accept().await?;

        tokio::spawn(async move {
            let mut buf = [0; 1024];

            println!(
                "Connection received from {:?}",
                socket.local_addr().unwrap()
            );

            loop {
                let n = match socket.read(&mut buf).await {
                    Ok(n) if n == 0 => return,
                    Ok(n) => n,
                    Err(e) => {
                        eprintln!("Failed to read from socket; err = {:?}", e);
                        return;
                    }
                };

                match get_command_string(String::from_utf8(buf.to_vec()).unwrap()) {
                    Some(command) => match command.as_str() {
                        "QUIT" => {
                            println!(
                                "Client from {:?} disconnected.",
                                socket.local_addr().unwrap()
                            );
                            return;
                        }
                        _ => (),
                    },
                    None => (),
                };

                if let Err(e) = socket
                    .write_all(format!("Received {} bytes: {:x?}\n", n, &buf[0..n]).as_bytes())
                    .await
                {
                    eprintln!("Failed to write to socket; err = {:?}", e);
                    return;
                }
            }
        });
    }
}
