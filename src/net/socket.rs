use tokio::net::TcpListener;

pub async fn create_listener(port: u16, secure: bool) -> Result<TcpListener, std::io::Error> {
    // TODO: Handle TLS option
    return match TcpListener::bind(format!("[::]:{}", port)).await {
        Ok(ln) => Ok(ln),
        Err(e) => Err(e),
    };
}
