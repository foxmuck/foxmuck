# Furrous Oxide MUCK (FOXmuck)
FOXmuck is a reimplementation of [fuzzball-muck](https://github.com/fuzzball-muck/fuzzball) (aka fbmuck) in Rust.

# Why?
I've been a consumer of fbmuck-based services for 23 or so years, now. I've been a programmer for even longer than that. I feel that continued development of fbmuck - which is most certainly still ongoing! - might be better served by a new language. The main reason, though, is that I want to learn Rust, and I learn best with a project I can set my mind to.

# How?
This development is going to occur in phases, and probably then in fits and starts as I can find the time.

1. Network connectivity, including connection security
2. Session management
3. Basic command handling - `say`, `pose`, etc.
4. Data storage - database format, storage, etc.
5. Basic interactivity - multiple users conversing in the same room
6. Object concepts - players, rooms, exits, things, parentage.
7. Built-in commands - `look`, `@dig`, `@create`, etc.
8. Properties
9. Environments (see also parentage from #4)
10. Permissions
11. In-game interpreter
    1. MUF (fbmuck compatibility)
    2. Lua (the new shit)
12. ?
13. Profit.
